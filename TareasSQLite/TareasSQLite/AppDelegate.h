//
//  AppDelegate.h
//  TareasSQLite
//
//  Created by Máster Móviles on 21/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

