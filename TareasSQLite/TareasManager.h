//
//  TareasManager.h
//  TareasSQLite
//
//  Created by Máster Móviles on 21/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "Tarea.h"

@interface TareasManager :DBManager
- (NSMutableArray *) listarTareas;
- (BOOL) insertarTarea:(Tarea *)tarea;
@end
