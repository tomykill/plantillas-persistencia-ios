//
//  Tarea.h
//  TareasSQLite
//
//  Created by Máster Móviles on 21/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Tarea : NSObject
@property NSInteger id;
@property NSString *titulo;
@property NSInteger prioridad;
@property NSDate *vencimiento;

@end
