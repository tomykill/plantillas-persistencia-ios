//
//  TareasManager.m
//  TareasSQLite
//
//  Created by Máster Móviles on 21/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "TareasManager.h"
#import "Tarea.h"
#import <sqlite3.h>

@interface TareasManager ()

@end

@implementation TareasManager


- (NSMutableArray *) listarTareas
{
    
    NSString *querySQL = @"SELECT * FROM tareas ORDER BY tareas.vencimiento";//orderby
    
    sqlite3_stmt *statment;
    NSMutableArray *listaTareas =[[NSMutableArray alloc]init];
    
    int result = sqlite3_prepare_v2(db, [querySQL UTF8String], -1, &statment, NULL);
    
    if(result == SQLITE_OK)
    {
        while (sqlite3_step(statment) == SQLITE_ROW) {
            
            int id = (int) sqlite3_column_int(statment, 0);
            char *titulo = (char *) sqlite3_column_text(statment, 1);
            int prioridad = (int) sqlite3_column_int(statment, 2);
            int unix_time = (int) sqlite3_column_int(statment, 3);
            
            Tarea *tarea = [[Tarea alloc]init];
            
            tarea.id = id;
            tarea.titulo = [[NSString alloc] initWithUTF8String:titulo];
            tarea.prioridad = prioridad;
            tarea.vencimiento = [[NSDate alloc]initWithTimeIntervalSince1970:unix_time];
            
            [listaTareas addObject:tarea];
     
        }
    }
    sqlite3_finalize(statment);
    return listaTareas;
}

- (BOOL) insertarTarea:(Tarea *)t
{
    
    //Tarea *t;
    BOOL dev;
    
    //t = [[Tarea alloc]init];
    
    NSString *querySQL = @"INSERT INTO tareas (titulo, prioridad,vencimiento) VALUES (?,?,?)";
    sqlite3_stmt *statement;
    sqlite3_prepare_v2(db, [querySQL UTF8String], -1, &statement, NULL);
    sqlite3_bind_text(statement, 1, [t.titulo UTF8String], -1, SQLITE_STATIC);
    sqlite3_bind_int(statement, 2, (int) t.prioridad);
    sqlite3_bind_int(statement, 3, [t.vencimiento timeIntervalSince1970]);
    int result = sqlite3_step(statement);
    if (result==SQLITE_DONE)
    {
        NSLog(@"Registro almacenado OK");
        dev = YES;
    }else
        dev = NO;
    
    return dev;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
