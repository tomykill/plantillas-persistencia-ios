//
//  MensajesViewController.m
//  PlantillaParse
//
//  Created by Otto Colomina Pardo on 21/1/15.
//  Copyright (c) 2015 Universidad de Alicante. All rights reserved.
//

#import "MensajesViewController.h"
#import <Parse/Parse.h>

@interface MensajesViewController ()

@property NSArray *mensajes;

@end

@implementation MensajesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self obtenerMensajes];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)pulsadoIntroEnMensaje:(id)sender {
    NSLog(@"El mensaje es: %@", self.campoMensaje.text);
    
}

- (IBAction)clickBotonLogout:(id)sender {
    NSLog(@"Se ha pulsado el botón 'Logout'");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)clickBotonRefresh:(id)sender {
    NSLog(@"Se ha pulsado el botón 'Refresh'");
    [self obtenerMensajes];
}

- (void) obtenerMensajes {
    }

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"Mensaje %d", indexPath.row];
    return cell;
}


@end
