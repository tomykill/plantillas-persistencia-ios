//
//  AppDelegate.h
//  PlantillaParse
//
//  Created by Otto Colomina Pardo on 14/1/15.
//  Copyright (c) 2015 Universidad de Alicante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

