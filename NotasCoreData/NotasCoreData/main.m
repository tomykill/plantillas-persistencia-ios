//
//  main.m
//  NotasCoreData
//
//  Created by Máster Móviles on 26/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
