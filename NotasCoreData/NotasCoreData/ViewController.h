//
//  ViewController.h
//  NotasCoreData
//
//  Created by Máster Móviles on 26/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *campoNota;
@property (weak, nonatomic) IBOutlet UILabel *campoFecha;


@end

