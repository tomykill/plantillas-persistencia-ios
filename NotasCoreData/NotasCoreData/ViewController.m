//
//  ViewController.m
//  NotasCoreData
//
//  Created by Máster Móviles on 26/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import <uikit/UIKit.h>

@interface ViewController ()

@end

@implementation ViewController
- (IBAction)guardarNota:(id)sender {
    
    
    //obtenemos el delegate, ya que es donde está el código de acceso a Core Data
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    
    //Para crear objetos persistentes necesitamos el contexto
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    //Vamos a crear un objeto gestionado por Core Data
    NSManagedObject *nuevaNota = [NSEntityDescription
                                  insertNewObjectForEntityForName:@"Nota"
                                  inManagedObjectContext:miContexto];
    
    [nuevaNota setValue:[NSDate date] forKey:@"fecha"];
    [nuevaNota setValue:self.campoNota.text forKey:@"texto"];
    
    NSError *error;
    [miContexto save:&error];
    
    if (!error) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mma"];
        NSString *stringDate = [dateFormatter stringFromDate:[NSDate date]];
        NSLog(@"%@", stringDate);
        
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Nota Guardada"
                                                          message:stringDate
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
        self.campoFecha.text = stringDate;
    }
    else {
        self.campoNota.text = error.description;
    }

    
}



- (IBAction)crearNota:(id)sender {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mma"];
    NSString *stringDate = [dateFormatter stringFromDate:[NSDate date]];
    
    self.campoFecha.text = [NSString stringWithFormat:@"Fecha: %@", stringDate];
    self.campoNota.text = @"Escribe una nota nueva ... ";

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
